
# Chaining Higher Order Functions: 
Higher Order function is a function that takes functions as arguments and returns functions as output. For example,`Array.prototype.map`,  `Array.prototype.filter`  and  `Array.prototype.reduce` are some Higher-Order functions built into the JavaScript language.

## Function Chaining: 
Function chaining is a pattern in JavaScript where many functions are invoked on a single object. Each function call returns an object. This allows the functions to be chained together in a single statement, without requiring variables to store the result of every function call. Function chaining simplifies the code.

## Examples
### Example 1

 ```javascript
const addTwoValues = (a, b) => a + b;
const checkIsEven = num => num % 2 === 0;

const data = [2, 3, 1, 5, 4, 6];

const evenValues = data.filter(isEven); // [2, 4, 6]
const evenSum = evenValues.reduce(add); // 12
```
As you can see in  the example above, we have two functions, **addTwoValues** - adds two numbers, and, **checkIsEven** - checks if the number is even. 

The purpose of the above program is to calculate the sum of even numbers in the array. So, first we call *Array.filter()* on the given data array, with the function *isEven* as callback, second, we call *Array.reduce()* on the array returned by first step and pass in the *add* function as callback. As we can see, this is a two step process, where the results of every step is stored in a variable and thus, it makes the task cumbersome, although there is no denying that the code is more readable, but for a developer storing the result of every step is a cumbersome task.

**Solving the above problem using chaining**
 ```javascript
const addTwoValues = (a, b) => a + b;
const checkIsEven = num => num % 2 === 0;

const data = [2, 3, 1, 5, 4, 6];
const evenSum = data.filter(checkIsEven).reduce(addTwoValues,0); // 12 
```
The sum of even values can be solved in a single step, as shown above.  *Array.filter(checkIsEven)* is chained to *Array.reduce(addTwoValues)* such that the output of *Array.filter(checkIsEven)* acts as an input for *Array.reduce(addTwoValues)*. This is called function chaining. This saves us the task of storing the result of the first step. 

### Example 2
Chaining *reduce()* and *filter()* to get the price of all vegetables in a grocery list
```javascript
const grocery = [
  { unit: "rice", price: 20 },
  { unit: "Beetroot", price: 30 },
  { unit: "pulse", price: 30 },
  { unit: "Capsicum", price: 40 },
  { unit: "onion", price: 50 },
  { unit: "wheat", price: 60 },
];
let vegetables = ['Beetroot', 'onion', 'Capsicum']
//an array of vegetables for reference

const isVeggie = (unit) => unit.vegetables === true //returns true if item is a vegetable
const totalPrice = (total,curr) => return total+curr.price //calculating total price of vegetables

let result = grocery.filter(isVeggie).reduce(totalPrice,0)
```
In the above example, the *Array.filter()* method filters all vegetables and passes it to the *Array.reduce()* function which calculates the price of all vegetables

### Example 3
Chaining Map(), reduce() and filter() and sort() to manage data

The task is to filter data according to active users, convert date to DD/MM/YYYY, sort data according to date and then calculate total balance

 ```javascript
 const data = [
 {isActive:false, balance: 3,513.91, registered:  "2016-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 4,913.91, registered:  "2017-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 5,983.91, registered:  "2019-09-26T06:30:35 -06:-30"},
 {isActive:false, balance: 6,193.91, registered:  "2020-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 7,673.91, registered:  "2008-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 8,873.91, registered:  "2070-09-26T06:30:35 -06:-30"},
 ];

const activeCheck = (elem) => elem.isActive == true //returns if isActive is true
const dateConvert = (data) => {
	let date = data.split("T")[0].split("-")
	return `${date[2]}/${date[1]}/${date[0]}`
}//converting date to DD/MM/YYYY

const totalBalance = (total,curr) => return total + curr.balance //calculates the total balance 
const sortByDate = (a,b) => new Date(a.registered) - new Date(b.registered); //sorting according to date

let result = data.filter(activeCheck)
                  .map((elem) => {elem.registered = dateConvert(elem.registered);
                      return elem })
                  .sort(sortByDate)
                  .reduce(totalBalance,0)
```
The above example highlights the usage of chaining. We first use *Array.filter()* on data to get back a list of active users, then we use *Array.map()* to convert the registered date field to the format DD/MM/YYYY, once the conversion is done, we sort the object according to the date property, before using *Array.reduce()* to get the totalBalance of active users. 

Notice how we did not have to store the result of each step or each function call. We take the returned value of one function as an argument to the next, this is function-chaining, where the functions are chained together and all the operations are performed on a single object.
