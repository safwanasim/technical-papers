# Array.sort()!
The **arr.sort()** method sorts the array in place, meaning it doesn't return a new array, it sorts the original array and returns it. By default, the sort method sorts elements alphabetically, it converts the arguments (excluding undefined) into strings first, then sorts them in the ascending order of the strings in UTF-16 code units.  To sort numeric values, we must pass in a callback function that handles numeric sort.

## Parameters:
-**Compare Function** (Optional): The compare function is a callback function that specifies the sorting order of the array to be returned. It takes in two arguments :

 1. **First Element:**  The First element for comparison
 2. **Second Element**:  The second element for comparison

## Syntax:
array.sort(compareFunction)
**Compare Function:**
 - compareFunction(a,b) < 0:
   Then the array is sorted in ascending order, that means element a comes before element b.
 - compareFunction(a,b)>0:
  Then the array is sorted in descending order, that means in the sorted array, the element b comes before element a  
 - compareFunction(a,b) = 0:
 In this case, the array is returned as it is, no sorting takes place on array

## Examples:
**Example 1**
Sorting array of numbers, without the compare function:

   ```javascript
var numArray = [140000, 104, 99];
numArray = numArray.sort();
console.log(numArray) //---> Output: [104, 140000, 99]
```
Even though the correct output for the above code should be [99,104,140000], we get the output sorted alphabetically. To fix this issue, we must pass in a compare function that handles numeric values:

 ```javascript
var numArray = [140000, 104, 99];
numArray.sort(function(a, b) {
  return a - b;
});
console.log(numArray); //---> Output : [99, 104, 140000]
```
MDN documentation recommends this compare function for arrays that don't contain Infinity or NaN. (Because  `Infinity - Infinity`  is NaN, not 0).

**Example 2**:
Sorting an array of objects based on the values of their properties
```javascript
var items = [
  { name: 'Asim', value: 01 },
  { name: 'Enos', value: 99 },
  { name: 'Aman', value: 36 },
  { name: 'Batman', value: -12 },
  { name: 'Superman', value: 00 },
  { name: 'Joker', value: 100 }
];

// sort by ascending order of values
items.sort(function (a, b) {
  return a.value - b.value;
}); 

// sort by ascending order of names
items.sort(function(a, b) {
  let nameA = a.name.toUpperCase(); // ignore upper and lowercase
  let nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  // names must be equal
  return 0;
});
```

**Example 3:**
Sorting an array of objects based on the date property:
```javascript
let array = [
{id: 1, date: Mar 12 2021 10:00:00 AM}, 
{id: 2, date: Mar 8 2021 08:00:00 AM},
{id: 3, date: Mar 3 2021 10:00:00 AM}
];

//sorting in descending order of dates
array.sort(function(a,b){  
  return new Date(b.date) - new Date(a.date);
  // Turns your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
});

//sorting in ascending order of dates
array.sort(function(a,b){
  return new Date(a.date) - new Date(b.date);
  // Turns your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
});

```

